import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:levant_test/features/draw_shapes/model/polygon.dart';

class PolygonSelection {
  static PolygonSelection instance = PolygonSelection._();

  PolygonSelection._();

  ValueNotifier<Polygon> selectedPolygon = ValueNotifier(Polygon([
    const Offset(0, 0),
    const Offset(100, 0),
    const Offset(100, 100),
    const Offset(0, 100),
  ]));

  // Polygon get selectedPolygon => _selectedPolygon;

  set selectPolygon(Polygon polygon) {
    selectedPolygon.value = polygon;
  }
}
