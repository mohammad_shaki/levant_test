import 'package:flutter/material.dart';
import 'package:levant_test/features/draw_shapes/model/polygon.dart';
import 'package:levant_test/features/draw_shapes/provider/polygon_selection.dart';
import 'package:levant_test/features/draw_shapes/widgets/polygon_canvas.dart';
import 'package:levant_test/features/draw_shapes/widgets/select_shape_button.dart';

class DrawPage extends StatelessWidget {
  const DrawPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Shapes')),
      floatingActionButton: const SelectShapeButton(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          const Text('Double Tap To Pin The object'),
          ValueListenableBuilder<Polygon>(
            valueListenable: PolygonSelection.instance.selectedPolygon,
            builder: (context, value, child) {
              return PolygonCanvas(key: ValueKey(value), polygon: value);
            },
          )
        ],
      ),
    );
  }
}
