import 'package:flutter/material.dart';
import 'package:levant_test/features/draw_shapes/provider/polygon_selection.dart';
import 'package:levant_test/features/draw_shapes/widgets/polygon_painter.dart';

import '../model/polygon.dart';
import '../model/random_shapes.dart';

class SelectShapeButton extends StatefulWidget {
  const SelectShapeButton({super.key});

  @override
  State<SelectShapeButton> createState() => _SelectShapeButtonState();
}

class _SelectShapeButtonState extends State<SelectShapeButton> {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: const Icon(Icons.format_shapes_rounded),
      onPressed: () {
        showPolygonSelectionDialog(context).then((value) {
          if (value != null) {
            PolygonSelection.instance.selectPolygon = value;
          }
        });
      },
    );
  }

  Future<Polygon?> showPolygonSelectionDialog(BuildContext context) {
    final customShapes = CustomShapes();
    return showDialog<Polygon>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Select a Polygon'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: customShapes.shaps.map((polygon) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  child: CustomPaint(
                    size: Size.fromHeight(150),
                    painter: PolygonPainter(polygon: polygon),
                  ),
                  onTap: () {
                    Navigator.of(context).pop(polygon);
                  },
                ),
              );
            }).toList(),
          ),
        );
      },
    );
  }
}
