import 'package:flutter/material.dart';
import 'package:levant_test/features/draw_shapes/model/polygon.dart';

class PolygonPainter extends CustomPainter {
  final Polygon polygon;
  final double scale;
  final Offset offset;
  final double rotation;
  final Offset rotationOrigin;

  PolygonPainter({
    required this.polygon,
    this.scale = 1.0,
    this.offset = Offset.zero,
    this.rotation = 0,
    this.rotationOrigin = Offset.zero,
  });

  @override
  void paint(Canvas canvas, Size size) {
    canvas.save();
    canvas.translate(size.width / 2, size.height / 2);
    canvas.scale(scale);
    canvas.translate(offset.dx, offset.dy);
    canvas.rotate(rotation);

    final Shader shader = const LinearGradient(
            colors: [Colors.black, Colors.white], stops: [0.1, 0.5])
        .createShader(Rect.fromLTWH(0.0, 0.0, size.width, size.height));

    final paint = Paint()
      ..color = Colors.blue
      ..strokeWidth = 2
      ..style = PaintingStyle.fill
      ..shader = shader;

    final path = Path();
    path.moveTo(polygon.vertices[0].dx, polygon.vertices[0].dy);
    for (var i = 1; i < polygon.vertices.length; i++) {
      path.lineTo(polygon.vertices[i].dx, polygon.vertices[i].dy);
    }
    path.close();
    canvas.drawPath(path, paint);

    canvas.restore();
  }

  @override
  bool shouldRepaint(PolygonPainter oldDelegate) {
    return true;
  }
}
