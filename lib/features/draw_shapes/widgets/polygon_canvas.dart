import 'package:flutter/material.dart';
import 'package:levant_test/features/draw_shapes/model/polygon.dart';
import 'package:levant_test/features/draw_shapes/widgets/polygon_painter.dart';

class PolygonCanvas extends StatefulWidget {
  final Polygon polygon;

  PolygonCanvas({Key? key, required this.polygon}) : super(key: key);

  @override
  _PolygonCanvasState createState() => _PolygonCanvasState();
}

class _PolygonCanvasState extends State<PolygonCanvas> {
  double _scale = 1.0;
  Offset _offset = Offset.zero;
  bool isPined = false;
  double _rotation = 0.0;
  Offset _rotationOrigin = Offset.zero;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onScaleStart: (details) {
        setState(() {
          _rotationOrigin = details.focalPoint;
        });
      },
      onDoubleTap: () {
        setState(() {
          isPined = !isPined;
        });
      },
      onScaleUpdate: (details) {
        setState(() {
          const sensitivityOffset = 0.8;
          const sensitivityScale = 0.01;
          if (details.rotation != 0) _rotation = details.rotation;
          if (!isPined) {
            if (details.scale > 1.0) {
              _scale += (details.scale - 1.0) * sensitivityScale;
            } else if (details.scale < 1.0) {
              _scale -= (1.0 - details.scale) * sensitivityScale;
            }
            _offset +=
                (details.focalPoint - _rotationOrigin) * sensitivityOffset;
          }
          _rotationOrigin = details.focalPoint;
        });
      },
      child: Container(
        color: isPined ? Theme.of(context).primaryColor : Colors.transparent,
        child: CustomPaint(
          size: Size.infinite,
          painter: PolygonPainter(
            polygon: widget.polygon,
            scale: _scale,
            offset: _offset,
            rotation: _rotation,
            rotationOrigin: _rotationOrigin,
          ),
        ),
      ),
    );
  }
}
