import 'dart:ui';

import 'package:levant_test/features/draw_shapes/model/polygon.dart';

class CustomShapes {
  final List<Polygon> _shaps = [
    Polygon([
      const Offset(0, 0),
      const Offset(100, 0),
      const Offset(50, 100),
    ]),
    Polygon([
      const Offset(0, 0),
      const Offset(75, 0),
      const Offset(75, 75),
      const Offset(0, 75),
    ]),
    Polygon([
      const Offset(50, 0),
      const Offset(100, 0),
      const Offset(50, 100),
      const Offset(0, 100),
    ]),
  ];

  List<Polygon> get shaps => _shaps;
}
