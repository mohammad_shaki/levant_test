import 'package:flutter/material.dart';

class Polygon {
  final List<Offset> vertices;

  Polygon(this.vertices);
}
