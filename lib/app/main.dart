import 'package:flutter/material.dart';
import 'package:levant_test/features/draw_shapes/presentation/draw_screen.dart';

void main() {
  runApp(const MaterialApp(home: DrawPage()));
}
